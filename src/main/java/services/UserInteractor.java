package services;

import domain.Group;
import domain.User;
import repositories.entities.UserRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.Date;
import java.util.LinkedList;

public class UserInteractor {
    private IEntityRepository useRepo;

    public UserInteractor() {
        useRepo = new UserRepository();
    }

    public User getUserById(long id) {
        LinkedList<User> users = (LinkedList<User>) useRepo.query(
                "SELECT * FROM users WHERE id = " + id);
        return (users.isEmpty() ? null : users.get(0));
    }

    public void addUser(String name,
                        String surname,
                        String username,
                        String password,
                        Date birthday) {
        User user = new User(name, surname, username, password, birthday);
        useRepo.add(user);

    }

    public void addGroup(String username, String groupName) {
        User user = new User(username);
        Group group = new Group(groupName);
        useRepo.add(user, group);
    }

    public void removeUser(String username) {
        User user = new User(username);
        useRepo.remove(user);
    }

    public LinkedList<Group> listGroups(String username) {
        User user = new User(username);
        LinkedList<Group> groups = (LinkedList<Group>) useRepo.listAll(user);

        return (groups.isEmpty() ? null : groups);
    }
}
