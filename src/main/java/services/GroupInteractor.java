package services;

import domain.Group;
import domain.User;
import repositories.entities.GroupRepository;
import repositories.interfaces.IEntityRepository;

import java.util.LinkedList;

public class GroupInteractor {
    private IEntityRepository groupRepo;

    public GroupInteractor() { groupRepo = new GroupRepository(); }

    public void addGroup(String name) {
        Group group = new Group(name);
        groupRepo.add(group);
    }

    public void removeGroup(String name) {
        Group group = new Group(name);
        groupRepo.remove(group);
    }

    public LinkedList<User> listUsers(String name) {
        Group group = new Group(name);
        LinkedList<User> users = (LinkedList<User>) groupRepo.listAll(group);

        return (users.isEmpty() ? null : users);
    }


}
