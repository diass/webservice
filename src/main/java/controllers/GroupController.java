package controllers;

import domain.Group;
import domain.User;
import org.glassfish.jersey.media.multipart.FormDataParam;
import repositories.entities.GroupRepository;
import repositories.interfaces.IEntityRepository;
import services.GroupInteractor;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;

@Path("groups")
public class GroupController {

    private GroupInteractor groupInteractor;

    public GroupController() {
        groupInteractor = new GroupInteractor();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response addGroup(@FormDataParam("name") String name) {
        groupInteractor.addGroup(name);
        return Response.status(200).entity("Group added").build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeGroup(@FormDataParam("name") String name) {
        groupInteractor.removeGroup(name);
        return Response.status(200).entity("Group deleted").build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/listUsers")
    public Response listUsers(@FormDataParam("name") String name) {
        LinkedList<User> users = groupInteractor.listUsers(name);

        if(users.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("This group doesn't have any user")
                    .build();
        } else {
            return Response.status(200).entity(users).build();
        }
    }

}
