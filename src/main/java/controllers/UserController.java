package controllers;

import domain.Group;
import domain.User;
import org.glassfish.jersey.media.multipart.FormDataParam;
import services.UserInteractor;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.util.LinkedList;

@Path("users")
public class UserController {
    private UserInteractor userInteractor;

    public UserController() {
        userInteractor = new UserInteractor();
    }

    @GET
    public String hello() {
        return "Hello world!";
    }

    @GET
    @Path("/{id}")
    public Response getUserByID(@PathParam("id") long id) {
        User user = userInteractor.getUserById(id);
        if (user == null)
            return  Response.status(Response.Status.NOT_FOUND)
                    .entity("There is no user with such id")
                    .build();
        else
            return Response.status(Response.Status.OK)
                    .entity(user)
                    .build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response addUser(@FormDataParam("name") String name,
                            @FormDataParam("surname") String surname,
                            @FormDataParam("username") String username,
                            @FormDataParam("password") String password,
                            @FormDataParam("birthday") Date birthday) {
        userInteractor.addUser(name, surname, username, password, birthday);
        return Response.status(200).entity("User created").build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/addGroup")
    public Response addGroup(@FormDataParam("username") String username,
                             @FormDataParam("groupname") String groupname) {
        userInteractor.addGroup(username, groupname);
        return Response.status(200).entity("Group added to user").build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/listGroups")
    public Response listGroups(@FormDataParam("username") String username) {
        LinkedList<Group> groups = userInteractor.listGroups(username);

        if(groups.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("This user is not a member of any group")
                    .build();
        } else {
            return Response.status(200).entity(groups).build();
        }
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeUser(@FormDataParam("username") String username) {
        userInteractor.removeUser(username);
        return Response.status(200).entity("User deleted").build();
    }

}
