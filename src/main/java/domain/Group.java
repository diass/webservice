package domain;

public class Group {
    private long id;
    private String name;

    public Group() {  }

    public Group(long id) {
        setId(id);
    }

    public Group(String name) {
        setName(name);
    }

    public Group(long id, String name) {
        this(id);
        setName(name);
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() { return id; }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return  name;
    }

    @Override
    public String toString() {
        return "Group{" + "id = " + id + ", name = '" + name + '}';
    }
}
