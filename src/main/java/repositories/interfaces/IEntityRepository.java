package repositories.interfaces;

public interface IEntityRepository<T, V> {
    void add(T entity);
    void add(T entity, V entity2);
    Iterable<V> listAll(T entity);
    void update(T entity);
    void remove(T entity);

    Iterable<T> query(String sql);
}

