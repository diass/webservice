package repositories.entities;

import domain.Group;
import domain.User;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class UserRepository implements IEntityRepository<User, Group> {

    private IDBRepository dbrepo;

    public UserRepository() {
        dbrepo = new PostgresRepository();
    }

    @Override
    public void add(User entity) {
        try {
            String sql = "INSERT INTO users(name, surname, username, password, birthday) " +
                    "VALUES(?,?,?,?,?)";
            PreparedStatement preparedStatement = dbrepo.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSurname());
            preparedStatement.setString(3, entity.getUsername());
            preparedStatement.setString(4, entity.getPassword());
            preparedStatement.setDate(5, entity.getBirthday());
            preparedStatement.execute();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void remove(User entity) {
        try{
            String sql = "DELETE FROM users WHERE username = ?";
            PreparedStatement preparedStatement = dbrepo.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, entity.getUsername());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void add(User user, Group group) { //Add group to the user
        try {
            long user_id = 0;
            long group_id = 0;

            String getUserIdSql = "SELECT id FROM users WHERE username = ?";
            PreparedStatement preparedUserId = dbrepo.getConnection().prepareStatement(getUserIdSql);
            preparedUserId.setString(1, user.getUsername());
            ResultSet userIdRs = preparedUserId.executeQuery();
            while (userIdRs.next()) {
                user_id = userIdRs.getLong(1);
            }

            String getGroupIdSql = "SELECT id FROM groups WHERE name = ?";
            PreparedStatement preparedGroupId = dbrepo.getConnection().prepareStatement(getGroupIdSql);
            preparedGroupId.setString(1, group.getName());
            ResultSet groupIdRs = preparedGroupId.executeQuery();
            while (groupIdRs.next()) {
                group_id = groupIdRs.getLong(1);
            }

            String sql = "INSERT INTO groups_mapping(user_id, group_id)" +
                    "VALUES(?,?)";
            PreparedStatement preparedStatement = dbrepo.getConnection().prepareStatement(sql);
            preparedStatement.setLong(1, user_id);
            preparedStatement.setLong(2, group_id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Iterable<Group> listAll(User user) { //List all groups of the user
        try {
            long user_id = 0;

            String getUserIdSql = "SELECT id FROM users WHERE username = ?";
            PreparedStatement preparedUserId = dbrepo.getConnection().prepareStatement(getUserIdSql);
            preparedUserId.setString(1, user.getUsername());
            ResultSet userIdRs = preparedUserId.executeQuery();
            while (userIdRs.next()) {
                user_id = userIdRs.getLong(1);
            }

            String selectInnerJoin = "SELECT * FROM groups_mapping INNER JOIN groups ON " +
                    "groups_mapping.group_id = groups.id WHERE user_id = ?";
            PreparedStatement preparedInnerJoin = dbrepo.getConnection().prepareStatement(selectInnerJoin);
            preparedInnerJoin.setLong(1, user_id);
            ResultSet groupsSet = preparedInnerJoin.executeQuery();
            LinkedList<Group> groups = new LinkedList<>();
            while (groupsSet.next()) {
                Group group = new Group(groupsSet.getString("name"));
                groups.add(group);
            }
            return groups;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<User> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<User> users = new LinkedList<>();
            while (rs.next()) {
                User user = new User(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday")
                );
                users.add(user);
            }
            return users;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}

