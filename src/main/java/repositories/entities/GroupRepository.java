package repositories.entities;

import domain.Group;
import domain.User;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class GroupRepository implements IEntityRepository<Group, User> {
    private IDBRepository dbrepo;

    public GroupRepository() {dbrepo = new PostgresRepository(); }

    @Override
    public void add(Group entity) {
        try {
            String sql = "INSERT INTO groups(name)" + "VALUES(?)";
            PreparedStatement preparedStatement = dbrepo.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void add(Group entity, User entity2) { //Add tutor to the group

    }

    @Override
    public Iterable<User> listAll(Group group) {
        try {
            long group_id = 0;

            String getGroupIdSql = "SELECT id FROM groups WHERE name = ?";
            PreparedStatement preparedGroupId = dbrepo.getConnection().prepareStatement(getGroupIdSql);
            preparedGroupId.setString(1, group.getName());
            ResultSet groupIdRs = preparedGroupId.executeQuery();
            while (groupIdRs.next()) {
                group_id = groupIdRs.getLong(1);
            }

            String selectInnerJoin = "SELECT * FROM groups_mapping INNER JOIN users ON " +
                    "groups_mapping.user_id = users.id WHERE group_id = ?";
            PreparedStatement preparedInnerJoin = dbrepo.getConnection().prepareStatement(selectInnerJoin);
            preparedInnerJoin.setLong(1, group_id);
            ResultSet usersSet = preparedInnerJoin.executeQuery();
            LinkedList<User> users = new LinkedList<>();
            while (usersSet.next()) {
                User user = new User(
                        usersSet.getString("name"),
                        usersSet.getString("surname"),
                        usersSet.getString("username"),
                        usersSet.getDate("birthday"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Group entity) {

    }

    @Override
    public void remove(Group entity) {
        try {
            String sql = "DELETE FROM groups WHERE name = ?";
            PreparedStatement preparedStatement = dbrepo.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Iterable query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Group> groups = new LinkedList<>();
            while (rs.next()) {
                Group group = new Group(
                        rs.getLong("id"),
                        rs.getString("name")
                );
                groups.add(group);
            }
            return groups;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
